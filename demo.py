#!/usr/bin/env python3

# Python script for use in a git tutorial.
#
# Users will create their own function bodies and learn
# to collaborate on this file using Git.
#
# Brendan Kristiansen <b@bek.sh>
# This code is licensed under the Mozilla Public License 2.0

#####
## Functions for people to collaborate on.
## Add more if necessary.
#####

## TODO: Comment here
## author: ???
def func1(a, b):
    return -1

## TODO: Comment here
## author: ???
def func2(a, b):
    return -1

## TODO: Comment here
## author: ???
def func3(a, b):
    return -1

## TODO: Comment here
## author: ???
def func4(a, b):
    return -1

## Main
## author: Brendan Kristiansen
def main():

    person1 = "Bob"
    person2 = "Sally"
    person3 = "Marge"
    person4 = "Nobody"

    # Constant declarations
    varA = 7
    varB = 8
    varC = 17
    varD = 18
    listA = [varA, varC]
    listB = [varB, varC]
    listC = [listA, listB]

    print("Var A", varA)
    print("Var B", varB)
    print("Var C", varC)
    print("Var D", varD)
    print("List A", listA)
    print("List B", listB)
    print("List C", listC)
    
    # Function calls
    res1 = func1(varA, varC)
    res2 = func2(varA, varD)
    res3 = func3(varB, varD)
    res4 = func4(listC, listA)

    # Results
    print(person1, " Result 1", res1)
    print(person2, " Result 2", res2)
    print(person3, " Result 3", res3)
    print(person4, " Result 4", res4)

####################

if __name__ == "__main__":
    main()
else:
    print("Cannot import this file as module")
