# Git Tutorial

Tutorial version 0.1.0

A short tutorial to walk a group through the basics of collaborating with git using the shell.
It is suggested you fork this repository and use that as a basis for your work.

The contents of this repository are licensed under the Mozilla Public License 2.0.

## Requirements

This tutorial requires the following:

* A working installation of Git
* A working installation of Python 3
* A basic knowledge of Python

## Instructions

This demo assumes a group of people are learning git with at least one person already knowledgeable
with Git serving as the leader. If you are an individual interested in learning Git, I suggest walking
through Code Academy's short lesson. These instructions are targeted toward the group leader as
a guide.

### Basics

1. First, all parties involved should clone this repository. It is important that everybody
	involved has write access to it. If possible, have everybody set up SSH here, so they
	may complete the tutorial over ssh instead of https.

2. Assign every party one of the empty functions to modify. Have them write a comment block for
	their function.

3. Instruct them through the process of staging and committing their changes.

4. After everybody has committed their changes, show everybody their different outputs of `git log`.
	Explain the meaning of the information on this screen.

5. Have everybody push their changes one at a time. It is suggested you refresh the commits page in
	Bitbucket or Github between every push.

6. Have everybody pull their changes. Look at your git logs again.

### Collaborating in Git

1. Instruct everybody to write something in their function body. It can be anything, but it does
	need to return some value.

2. After everybody has completed their functions and tested that they work, everyone should commit
	and push their changes. The leader should delete functions and variables that are not being
	used and push those changes as well. If none of the deleted functions or variables are in
	use, this should not cause the program to malfunction and should not cause a conflict within
	Git.

3. Instruct everybody to pull the latest set of changes from the repo. After everybody has run the
	code and verified that it works. The leader should demonstrate the creation of a tag at this
	point.

### Rolling back changes

1. After tags have been explained, the leader should go into the source code and beak one user's
	function, but DO NOT COMMIT. Show the other group how to revert changes pre-commit using
	`git checkout`.

2. Change another part of the source code that will leave it in a working state. Run it for the rest
	of the group. Show them how to temorarily roll back uncommitted changes using `git stash` and
	`git stash pop`.

3. After those changes have been restored, commit them and push. Then assume you did not want these
	changes to happen. Teach the group to undo that commit using `git revert`. Show them what that
	looks like in the log (There was a reversion made early in this repository's history as an
	example.)

### Branches

1. Instruct everybody to create their own branch. Discuss naming conventions for branches if appropriate.

2. Once everybody is on their own branch, instruct them to improve their code in some way. Ideally, each
	person will make multiple commits. After their first commit, they should be taught how to push
	a new branch to Origin.

3. After everybody has finished the changes they want to make on their branch, select half the group
	and walk through the process of merging to master from the shell. It is suggested you merge
	the copy of their branch located on Origin.

4. For the other half, log into the repository's online UI and walk through the process of creating a
	pull request and merging that way.

### Resolving conflicts

1. Instruct everybody to checkout master and pull the most recent version after all branches have been
	merged. Instruct one person to open a new branch and make a change to the code. Push that branch.

2. Instruct another member of the group to make a different change to the same line of code in Master.
	Push that change to origin/master.

3. Pull the most recent version of master and attempt to merge the branch with the other version of
	code. Explain the error and show how to resolve the problem.

### Advanced collaboration

1. Instruct each user to open a new branch and begin making changes. While this is happening, create
	a new file and update the .gitignore file and push both changes to master as separate commits
	to master.

*It is important to understand the difference between cherry-pick and checkout here*

2. Instruct the group to accept one of the changesets into their branches using cherry-pick. It is
	recommended you update the .gitignore using this method.

3. Instruct the group to checkout the other file to their branch. Everybody should push their branches
	to Origin. The instructor should pull, merge them into master, then push to origin.

## Feedback

Contact Brendan Kristiansen with any comments or suggestions for this demo at b@bek.sh. Of course, you
can also always fork and submit a pull request.
